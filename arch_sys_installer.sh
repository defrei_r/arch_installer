#!/bin/bash
echo -n "Quelle commande utiliser pour l'éditeur de texte ? "
read CMD_EDITOR

echo -n "Souhaitez vous configurer la locale ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    echo "Configuration du fichier /etc/locale.gen..."
    $CMD_EDITOR /etc/locale.gen
    echo "Exécution de locale-gen..."
    locale-gen
    echo "Configuration du fichier /etc/locale.conf..."
    echo "Par défaut : LANG=\"fr_FR.UTF-8\""
    read
    $CMD_EDITOR /etc/locale.conf
fi

echo -n "Souhaitez vous configurer les partitions composant votre système ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    echo "Configuration du fichier /etc/fstab..."
    read
    $CMD_EDITOR /etc/fstab
fi

echo -n "Souhaitez vous configurer le nom de votre machine ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    echo "Configuration du fichier /etc/hostname..."
    read
    $CMD_EDITOR /etc/hostname
    echo "Configuration du fichier /etc/hosts..."
    read
    $CMD_EDITOR /etc/hosts
fi

echo -n "Souhaitez vous configurer la disposition de votre clavier ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    echo "Configuration du fichier /etc/vconsole.conf..."
    echo "Par défaut : KEYMAP=fr-pc"
    read
    $CMD_EDITOR /etc/vconsole.conf
    echo "Création du fichier /etc/X11/xorg.conf.d/10-keyboard-layout.conf..."
    cat > /etc/X11/xorg.conf.d/10-keyboard-layout.conf <<EOF
Section "InputClass"
Identifier         "Keyboard Layout"
    MatchIsKeyboard    "yes"
    MatchDevicePath    "/dev/input/event*"
    Option             "XkbLayout"  "fr"
    Option             "XkbVariant" "latin9"
EndSection
EOF
fi

echo -n "Souhaitez vous configurer le fuseau horaire ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    echo "Création du lien symbolique..."
    ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
fi

echo -n "Souhaitez vous enlever le beep ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    echo "Création du fichier /etc/modprobe.d/blacklist.conf..."
    echo "blacklist pcspkr" > /etc/modprobe.d/blacklist.conf
fi

echo -n "Souhaitez vous configurer la WiFi iit-wifi ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    echo "Création du fichier /etc/wpa_supplicant/wpa_iit-wifi.conf..."
    echo -n "Votre login (login_l) : "
    read login
    echo -n "Votre mot de passe SOCKS : "
    read password
    cat > /etc/wpa_supplicant/wpa_iit-wifi.conf <<EOF
ctrl_interface=/var/run/wpa_supplicant
eapol_version=1
ap_scan=1
fast_reauth=1

network={
ssid="iit-wifi"
scan_ssid=1
key_mgmt=WPA-EAP
eap=LEAP
identity="$login"
password="$password"
}
EOF
    echo "Création du fichier /etc/network.d/iit-wifi..."
    cat > /etc/network.d/iit-wifi <<EOF
CONNECTION='wireless'
DESCRIPTION='iit-wifi'
INTERFACE='wlo1'
SECURITY='wpa-config'
WPA_CONF='/etc/wpa_supplicant/wpa_iit-wifi.conf'
IP='dhcp'
DHCP_TIMEOUT=60
HIDDEN=yes
EOF
fi


echo -n "Souhaitez vous configurer pacman  ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    echo "Configuration du fichier /etc/pacman.d/mirrorlist..."
    echo "Décommentez les lignes nécéssaires du fichier qui va s'ouvrir."
    read
    nano /etc/pacman.d/mirrorlist
    echo "Exécution de pacam --init... (cela peut prendre jusqu'à 10 minutes)"
    pacman-key --init
    echo "Exécution de pacman --populate archlinux..."
    pacman-key --populate archlinux
fi

echo "Exécution du ramdisk..."
mkinitcpio -p linux

echo "Configuration du mot de passe ROOT..."
passwd



