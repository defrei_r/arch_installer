#!/bin/bash
if [ "$USER" != "root" ]
then
    echo "Démarrez le script en sudo."
    exit
fi
echo "
                             +
                            +++
                           =+++=
                          ,+++++:
                          =++++++
                         +++++++++
                        +++++++++++
                       ++++++++++++=
                       +++++++++++++
                        ~++++++++++++
                          ++++++++++++
                           ++++++++++++
                   ~++      +++++++++++~
                   ++++++    +++++++++++
                  ++++++++++: +++++++++++
                 =++++++++++++++++++++++++
                =+++++++++++++++++++++++++=
               ,+++++++++++++++++++++++++++,
               ++++++++++++++============+++
              =++++++++======================
             ++++++===========================
            =++================================
           ,===================================,
           ================,   ,================
          ===============         ===============
         ==============             =====:    ====
        ==============               =======
        =============:               :========
       ==============                 ===========
      ==============~                  =============
     ===============                   ===============
    ~===============                   ===============~
    ================                   ================
   ================                     ================
  =================                     =================
 ==================                     ==================
 Bienvenue sur ArchLinux Installer - par Raphael DE FREITAS
"
echo "Ce script part du principe que :
- Vous disposez d'une connexion internet fonctionnelle.
- Vous disposez de toutes les partitions prêtes à l'emploi.
- Tout est à vos risques et périls."

echo -n "Êtes vous prêt(e) ? [O/n] "
read on
if test x"$on" = xn;
then
    exit;
fi

SCRIPT_DIR=$(readlink -e $(dirname $0))

### ETAPE 1 ###
echo ""
echo "ETAPE 1: Création du système minimal"
ARCH_MINI="$PWD/tmp"
ARCHI=$(uname -m)
read

echo "Créations du dossier pour le système minimal..."
rm -rf $ARCH_MINI
mkdir $ARCH_MINI
cd $ARCH_MINI

echo "Téléchargement de l'archive contenant le système minimal..."
curl -O http://mir.archlinux.fr/~tuxce/chroot/archlinux.chroot.$ARCHI.tgz

echo "Extraction de l'archive dans le dossier du système minimal..."
tar -zxf archlinux.chroot.$ARCHI.tgz
rm -rf archlinux.chroot.$ARCHI.tgz

echo "Montage des systèmes de fichier spéciaux..."
mount -B /proc "$ARCH_MINI/proc"
mount -B /dev "$ARCH_MINI/dev"
mount -B /sys "$ARCH_MINI/sys"

### ETAPE 2 ###
echo ""
echo "ETAPE 2: Préparation du système ArchLinux"
ARCH_SYS="$ARCH_MINI/mnt"
rm -rf $ARCH_SYS
read

echo -n "Quel est le nom de la partition racine \"/\" : "
read ARCH_ROOT

echo -n "Souhaitez vous formatter la partition ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    echo "Formatage de la partition racine..."
    mkfs.ext4 "$ARCH_ROOT"
fi

echo "Montage de la partition racine ArchLinux..."
mkdir "$ARCH_SYS"
mount "$ARCH_ROOT" "$ARCH_SYS"

echo "Création des dossiers nécéssaires..."
mkdir -p "$ARCH_SYS"/var/{cache/pacman/pkg,lib/pacman} "$ARCH_SYS"/{dev,proc,sys,run,tmp,etc,boot,root}

echo "Montage des systèmes de fichier spéciaux..."
mount -B /proc "$ARCH_SYS/proc"
mount -B /dev "$ARCH_SYS/dev"
mount -B /sys "$ARCH_SYS/sys"

### ETAPE 3 ###
echo ""
echo "Etape 3: Installation de ArchLinux via le système minimal"
read

echo "Préparation du chroot..."
cp "$SCRIPT_DIR/arch_mini_installer.sh" "$ARCH_MINI/root/arch_mini_installer.sh"

echo "Accès au système minimal par chroot..."
chroot "$ARCH_MINI" /bin/bash /root/arch_mini_installer.sh

### ETAPE 4 ###
echo ""
echo "Etape 4: Configuration de ArchLinux"
read

echo "Préparation du chroot..."
cp "$SCRIPT_DIR/arch_sys_installer.sh" "$ARCH_SYS/root/arch_sys_installer.sh"

echo "Accès au système ArchLinux par chroot..."
chroot "$ARCH_SYS" /bin/bash /root/arch_sys_installer.sh

### FIN ###
echo "Suppression des scripts d'installation..."
rm -rf "$ARCH_SYS/root/arch_sys_installer.sh"
rm -rf "$ARCH_MINI/root/arch_mini_installer.sh"

echo -n "Souhaitez vous mettre à jour GRUB2 ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    echo "Mise à jour de GRUB2..."
    update-grub2
fi

echo "Démontage des fichiers spéciaux..."
umount "$ARCH_SYS/"{dev,proc,sys}

echo "Démontage de la partition racine ArchLinux..."
umount "$ARCH_SYS"

echo "Démontage des fichiers spéciaux..."
umount "$ARCH_MINI/"{dev,proc,sys}

echo "Suppression du dossier du système minimal..."
rm -rf "$ARCH_MINI"

