#!/bin/bash
CMD="pacman -r /mnt --cachedir /mnt/var/cache/pacman/pkg -Sy"
PACKETS_BASE="base base-devel"
PACKETS_NETWORK="wireless_tools net-tools netcfg dialog wpa_supplicant networkmanager"
PACKETS_XORG="xorg-server xorg-xinit xorg-server-utils"
PACKETS_DRIVERS="synaptics xf86-video-ati pulseaudio alsa-utils"
PACKETS_DEV="htop git subversion openssh emacs valgrind cloc"
PACKETS_ADD=""

#1 3-6 8 10-12 14-17 20-26 29 30 32-37 44-46 48 49
#1-3 6 7 10-12 14 17 18 20 22-24

echo "Installation des paquets de base..."
$CMD $PACKETS_BASE

echo -n "Souhaitez vous installer les paquets réseau ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    $CMD $PACKETS_NETWORK
fi

echo -n "Souhaitez vous installer les paquets X.Org ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    $CMD $PACKETS_XORG
fi

echo -n "Souhaitez vous installer les paquets drivers ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    $CMD $PACKETS_DRIVERS
fi

echo -n "Souhaitez vous installer les paquets pour le développement ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    $CMD $PACKETS_DEV
fi

echo -n "Souhaitez vous installer d'autres paquets ? [O/n] "
read on
if test x"$on" = x -o x"$on" = xo -o x"$on" = xO;
then
    echo -n "Indiquez le nom des paquets : "
    read PACKETS_ADD
    if test x"$PACKETS_ADD" != x;
    then
	$CMD $PACKETS_ADD
    fi
fi
